# frozen_string_literal: true

require 'i18n'
require 'uri'
require 'active_support'
require 'active_support/core_ext/module/delegation'
require 'active_support/core_ext/date/calculations'
require 'active_support/core_ext/time/calculations'
require 'graphlient'

I18n.load_path += Dir["#{File.expand_path(File.join('../config/locales'), File.dirname(__FILE__))}/*.yml"]

require_relative 'value_stream_dashboard/config'
require_relative 'value_stream_dashboard/runner'
require_relative 'value_stream_dashboard/issue_creator'
require_relative 'value_stream_dashboard/column'
require_relative 'value_stream_dashboard/metrics_collector'
require_relative 'value_stream_dashboard/formatters/git_lab_flavored_markdown'
require_relative 'value_stream_dashboard/api/queries'
require_relative 'value_stream_dashboard/api/client'

module ValueStreamDashboard
end
