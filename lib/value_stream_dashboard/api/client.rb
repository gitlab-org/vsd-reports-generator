# frozen_string_literal: true

module ValueStreamDashboard
  module API
    class Client
      DEFAULT_YAML_FILE = '.gitlab/analytics/dashboards/value_streams/value_streams.yaml'

      attr_reader :config, :client

      def initialize(config)
        @config = config
        @client = Graphlient::Client.new("#{config[:base_url]}/api/graphql", headers: {
                                                                               'User-Agent' => 'Value Stream Dashboard Ruby client',
                                                                               'Authorization' => "Bearer #{config[:access_token]}"
                                                                             },
                                                                             http_options: {
                                                                               read_timeout: 30,
                                                                               write_timeout: 30
                                                                             })
      end

      def create_issue(title:, description:)
        variables = {
          projectPath: config[:open_issue_in_project_path],
          title:,
          description:
        }

        client.query(Queries::CREATE_ISSUE_MUTATION, variables)
      end

      def read_config_file
        project_path = config.fetch(:project_path)
        yml_path = config.fetch(:yml_path)

        variables = {
          projectPath: project_path,
          ymlPath: yml_path
        }

        response = client.query(Queries::CONFIG_FILE_QUERY, variables)
        project = verify_project(response.data.project, project_path)
        verify_blob(project.repository.blobs.nodes.first, project_path, yml_path).raw_blob
      end

      def new_issues(project:, **args)
        issue_count = if project
                        response = client.query(Queries::PROJECT_NEW_ISSUES_QUERY, args)
                        response.to_h.dig('data', 'project', 'flowMetrics', 'issueCount', 'value')
                      else
                        response = client.query(Queries::GROUP_NEW_ISSUES_QUERY, args)
                        response.to_h.dig('data', 'group', 'flowMetrics', 'issueCount', 'value')
                      end

        issue_count.to_i if issue_count
      end

      def lead_time(project:, **args)
        lead_time = if project
                      response = client.query(Queries::PROJECT_LEAD_TIME_QUERY, args)
                      response.to_h.dig('data', 'project', 'flowMetrics', 'leadTime', 'value')
                    else
                      response = client.query(Queries::GROUP_LEAD_TIME_QUERY, args)
                      response.to_h.dig('data', 'group', 'flowMetrics', 'leadTime', 'value')
                    end

        lead_time.to_f.round(1) if lead_time
      end

      def cycle_time(project:, **args)
        cycle_time = if project
                       response = client.query(Queries::PROJECT_CYCLE_TIME_QUERY, args)
                       response.to_h.dig('data', 'project', 'flowMetrics', 'cycleTime', 'value')
                     else
                       response = client.query(Queries::GROUP_CYCLE_TIME_QUERY, args)
                       response.to_h.dig('data', 'group', 'flowMetrics', 'cycleTime', 'value')
                     end

        cycle_time.to_f.round(1) if cycle_time
      end

      def deployment_frequency(project:, **args)
        dora = dora_metrics_query(project:, args:)
        dora['deploymentFrequency'].to_f.round(1)
      end

      def lead_time_for_changes(project:, **args)
        dora = dora_metrics_query(project:, args:)
        from_seconds_to_days(dora['leadTimeForChanges'].to_f).round(1)
      end

      def time_to_restore_service(project:, **args)
        dora = dora_metrics_query(project:, args:)
        from_seconds_to_days(dora['timeToRestoreService'].to_f).round(1)
      end

      def change_failure_rate(project:, **args)
        dora = dora_metrics_query(project:, args:)
        dora['changeFailureRate'].to_f.round(1)
      end

      def project?(path)
        # From the given path we cannot determine if it's a group or project so
        # we call the `projects` query and if it returns data we'll consider it a
        # project.
        response = client.query(Queries::PROJECT_FINDER_QUERY, { fullPath: path })
        response.to_h.dig(*%w(data project id)).present?
      end

      def self.build(config)
        new(config)
      end

      private

      def from_seconds_to_days(value)
        value.fdiv(60).fdiv(60).fdiv(24)
      end

      def dora_metrics_query(project:, args:)
        # Different results memoised for each set of date/project/group arguments
        # to save heaps of queries.
        @dora_metrics ||= {}
        @dora_metrics[args] ||= {}
        return @dora_metrics[args] unless @dora_metrics[args].blank?

        args[:from] = args[:from].iso8601
        args[:to] = args[:to].iso8601

        result = if project
                   response = client.query(Queries::PROJECT_DORA_METRICS_QUERY, args)
                   response.to_h.dig('data', 'project', 'dora', 'metrics')
                 else
                   response = client.query(Queries::GROUP_DORA_METRICS_QUERY, args)
                   response.to_h.dig('data', 'group', 'dora', 'metrics')
                 end

        @dora_metrics[args] = result.first || {}
      end

      def verify_project(node, project_path)
        return node if node

        raise <<~MESSAGE
          Error
          Couldn't access project: "#{project_path}", please check the following:

          - The used access token should have the required access level to read the project.
          - The given project path is spelled correctly.
        MESSAGE
      end

      def verify_blob(node, project_path, yml_path)
        return node if node

        custom_path = yml_path != DEFAULT_YAML_FILE

        raise <<~MESSAGE
          Error
          Couldn't access the #{custom_path ? 'provided' : 'default'} config file: "#{yml_path}" within the repository, please check the following:

          - The used access token should have access to read files from the repository.
          - The "#{yml_path}" file is present within the "#{project_path}" project.
        MESSAGE
      end
    end
  end
end
