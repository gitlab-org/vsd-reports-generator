# frozen_string_literal: true

module ValueStreamDashboard
  module API
    module Queries
      CONFIG_FILE_QUERY = <<-GRAPHQL
      query($projectPath: ID!, $ymlPath: String!) {
        project(fullPath: $projectPath) {
          repository {
            blobs(paths: [$ymlPath]) {
              nodes {
                rawBlob
              }
            }
          }
        }
      }
      GRAPHQL

      PROJECT_NEW_ISSUES_QUERY = <<-GRAPHQL
      query($fullPath: ID!, $from: Time!, $to: Time!) {
        project(fullPath: $fullPath) {
          flowMetrics {
            issueCount(from: $from, to: $to) {
              value
            }
          }
        }
      }
      GRAPHQL

      GROUP_NEW_ISSUES_QUERY = <<-GRAPHQL
      query($fullPath: ID!, $from: Time!, $to: Time!) {
        group(fullPath: $fullPath) {
          flowMetrics {
            issueCount(from: $from, to: $to) {
              value
            }
          }
        }
      }
      GRAPHQL

      PROJECT_FINDER_QUERY = <<-GRAPHQL
      query($fullPath: ID!) {
        project(fullPath: $fullPath) {
          id
        }
      }
      GRAPHQL

      CREATE_ISSUE_MUTATION = <<-GRAPHQL
      mutation($projectPath: ID!, $title: String!, $description: String!) {
        createIssue(input: { projectPath: $projectPath, title: $title, description: $description, confidential: true }) {
          issue {
            id
          }
        }
      }
      GRAPHQL

      GROUP_LEAD_TIME_QUERY = <<-GRAPHQL
      query($fullPath: ID!, $from: Time!, $to: Time!) {
        group(fullPath: $fullPath) {
          flowMetrics {
            leadTime(from: $from, to: $to) {
              value
            }
          }
        }
      }
      GRAPHQL

      PROJECT_LEAD_TIME_QUERY = <<-GRAPHQL
      query($fullPath: ID!, $from: Time!, $to: Time!) {
        project(fullPath: $fullPath) {
          flowMetrics {
            leadTime(from: $from, to: $to) {
              value
            }
          }
        }
      }
      GRAPHQL

      GROUP_CYCLE_TIME_QUERY = <<-GRAPHQL
      query($fullPath: ID!, $from: Time!, $to: Time!) {
        group(fullPath: $fullPath) {
          flowMetrics {
            cycleTime(from: $from, to: $to) {
              value
            }
          }
        }
      }
      GRAPHQL

      PROJECT_CYCLE_TIME_QUERY = <<-GRAPHQL
      query($fullPath: ID!, $from: Time!, $to: Time!) {
        project(fullPath: $fullPath) {
          flowMetrics {
            cycleTime(from: $from, to: $to) {
              value
            }
          }
        }
      }
      GRAPHQL

      GROUP_DORA_METRICS_QUERY = <<-GRAPHQL
      query($fullPath: ID!, $from: Date!, $to: Date!) {
        group(fullPath: $fullPath) {
          dora {
            metrics(startDate: $from, endDate: $to, interval: MONTHLY) {
              changeFailureRate
              deploymentFrequency
              leadTimeForChanges
              timeToRestoreService
            }
          }
        }
      }
      GRAPHQL

      PROJECT_DORA_METRICS_QUERY = <<-GRAPHQL
      query($fullPath: ID!, $from: Date!, $to: Date!) {
        project(fullPath: $fullPath) {
          dora {
            metrics(startDate: $from, endDate: $to, interval: MONTHLY) {
              changeFailureRate
              deploymentFrequency
              leadTimeForChanges
              timeToRestoreService
            }
          }
        }
      }
      GRAPHQL
    end
  end
end
