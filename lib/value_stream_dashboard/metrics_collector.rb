# frozen_string_literal: true

module ValueStreamDashboard
  class MetricsCollector
    METRICS_TO_COLLECT = {
      new_issues: {}.freeze,
      deployment_frequency: {
        format: 'days'
      }.freeze,
      lead_time_for_changes: {
        format: 'days'
      }.freeze,
      time_to_restore_service: {
        format: 'days'
      }.freeze,
      change_failure_rate: {
        format: 'percent'
      }.freeze,
      lead_time: {
        format: 'days'
      }.freeze,
      cycle_time: {
        format: 'days'
      }.freeze
    }.freeze

    PAST_MONTHS = 2

    def initialize(panel:, http_client:)
      @panel = panel
      @http_client = http_client
    end

    def headers
      [@panel['title'] || @panel['data']['namespace']] + date_headers
    end

    def data
      @data ||= METRICS_TO_COLLECT.map do |metric_name, config|
        columns = [I18n.t("metrics.#{metric_name}")]

        date_headers.each do |start_date, end_date|
          raw_value = @http_client.public_send(
            metric_name,
            fullPath: @panel['data']['namespace'],
            from: start_date,
            to: end_date,
            project: project?
          )

          columns << Column.new(raw_value:, config:)
        end

        columns
      end
    end

    private

    def project?
      return @project if defined?(@project)

      @project = @http_client.project?(@panel['data']['namespace'])
    end

    def date_headers
      current_date = Time.current.utc

      columns = [[current_date.beginning_of_month, current_date]]

      # +1 because we want to calculate % change using the previous month
      (PAST_MONTHS + 1).times do
        current_date = current_date.prev_month
        columns << [current_date.beginning_of_month.beginning_of_day, current_date.end_of_month.end_of_day]
      end

      columns
    end
  end
end
