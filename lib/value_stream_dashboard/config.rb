# frozen_string_literal: true

module ValueStreamDashboard
  class Config
    def initialize(options = {})
      @options = options
      uri = URI.parse(options.fetch(:project_url))
      @options[:base_url] = "#{uri.scheme}://#{uri.host}:#{uri.port}"
      @options[:project_path] = uri.path.gsub(/^\//, '')
      @options[:http_client] = API::Client.build(self)
    end

    delegate :[], :fetch, to: :@options

    def dashboard_config
      @dashboard_config ||= YAML.safe_load(options.fetch(:http_client).read_config_file)
    end

    def self.build
      options = {
        project_url: ENV.fetch('VSD_CONFIG_PROJECT_URL'),
        access_token: ENV.fetch('ACCESS_TOKEN'),
        yml_path: ENV['DASHBOARD_YAML_PATH'].presence || ValueStreamDashboard::API::Client::DEFAULT_YAML_FILE,
        open_issue_in_project_path: ENV['OPEN_ISSUE_IN_PROJECT_PATH'].presence,
        open_issue_title_prefix: ENV['OPEN_ISSUE_TITLE_PREFIX'].presence,
        open_issue_label_list: ENV['OPEN_ISSUE_LABEL_LIST'].presence,
        open_issue_assignee_list: ENV['OPEN_ISSUE_ASSIGNEE_LIST'].presence
      }.compact

      new(options)
    end

    def create_issue?
      @options[:open_issue_in_project_path]
    end

    private

    attr_reader :options
  end
end
