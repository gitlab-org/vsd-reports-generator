# frozen_string_literal: true

module ValueStreamDashboard
  class Column
    attr_reader :raw_value

    def initialize(raw_value:, config:)
      @raw_value = raw_value
      @config = config
    end

    def formatted_value
      return '' if raw_value.nil?
      return raw_value.to_s unless config[:format]

      I18n.t("formats.#{config[:format]}", value: raw_value)
    end

    private

    attr_reader :config
  end
end
