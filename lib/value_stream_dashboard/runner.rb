# frozen_string_literal: true

module ValueStreamDashboard
  class Runner
    attr_reader :config

    def initialize
      @config = Config.build
    end

    def run
      metrics = config.dashboard_config['panels'].map do |panel|
        MetricsCollector.new(panel:, http_client: config[:http_client])
      end

      markdown = Formatters::GitLabFlavoredMarkdown.new(metrics:).format
      File.write('report.md', markdown)

      IssueCreator.new(config:, table_content: markdown).call if config.create_issue?
    end
  end
end
