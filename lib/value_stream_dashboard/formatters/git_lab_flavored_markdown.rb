# frozen_string_literal: true

module ValueStreamDashboard
  module Formatters
    class GitLabFlavoredMarkdown
      def initialize(metrics:)
        @metrics = metrics
      end

      def format
        metrics.flat_map do |collector|
          [
            format_headers(collector),
            build_separator_line(collector),
            format_rows(collector),
            ''
          ]
        end.join("\n")
      end

      private

      attr_reader :metrics

      def build_separator_line(collector)
        separator = headers_for(collector).map { '-' }.join('|')

        "|#{separator}|"
      end

      def format_rows(collector)
        collector.data.map do |row|
          "|#{format_row(row)}|"
        end
      end

      def format_row(row)
        title, *values = row

        formatted_values = values.each_with_index.filter_map do |value, i|
          # skip rendering the last column
          format_value(value, values[i + 1]) unless i + 1 == values.size
        end

        [title, *formatted_values].join('|')
      end

      def format_value(value, previous_value)
        return value.formatted_value if missing_values?(value, previous_value)

        percentage_change = ((1 - value.raw_value.fdiv(previous_value.raw_value)) * -100).round(1)
        percentage = if percentage_change.positive?
                       "{+ +#{percentage_change}% +}"
                     elsif percentage_change.negative?
                       "{- #{percentage_change}% -}"
                     else
                       ''
                     end
        "#{value.formatted_value} #{percentage}"
      end

      def format_headers(collector)
        header_line = headers_for(collector).map { |header| format_header(header) }.join('|')

        "|#{header_line}|"
      end

      def format_header(value)
        return value if value.is_a?(String)

        end_date = value.last.to_date
        date_range = "#{value.first.to_date} - #{end_date}"
        if end_date == end_date.end_of_month
          date_range
        else
          I18n.t('month_to_date', date_range:)
        end
      end

      def headers_for(collector)
        # The last column is skipped, we only use it for percentage calculation
        collector.headers[0..-2]
      end

      def missing_values?(value, previous_value)
        value.nil? || value.raw_value.nil? || value.raw_value.zero? || previous_value.nil? || previous_value.raw_value.nil? || previous_value.raw_value.zero?
      end
    end
  end
end
