# Value Streams Dashboard Scheduled Reports

The Value Streams Dashboard Scheduled Reports tool is a [CI/CD component](https://docs.gitlab.com/ee/ci/components/) which allows you to periodically schedule a report with the most recent metrics from the [Value Streams Dashboard](https://docs.gitlab.com/ee/user/analytics/value_streams_dashboard.html) feature.

The tool collects metrics from projects or groups via the public GitLab GraphQL API then builds a report using the GitLab Flavored Markdown format. As the last step, an issue will be opened in the designated project.

## How to use the CI/CD component

### Requirements

- A GitLab project that contains the Value Streams Dashboard configuration YAML file. See the example [YAML configuration docs](https://docs.gitlab.com/ee/user/analytics/value_streams_dashboard.html#using-yaml-configuration).
- A GitLab project that has a configured `.gitlab-ci.yml` file to run the CI/CD component.
- An access token that can read the configured projects and groups, and open an issue in the designated project.
  - [Personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
  - [Group access token](https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html)

### Add the component

In the `.gitlab-ci.yml` file, add the following snippet:

```yaml
include:
  - component: gitlab.com/gitlab-org/vsd-reports-generator/vsd-reports-generator@main
    inputs:
      vsd_project_config_url: "https://gitlab.com/my-group/vsd-config-project"
      api_key: $API_KEY
      open_issue_in_project_path: "my-group/my-subgroup/project-for-reports"
      open_issue_assignee_list: "\"@my_user @another_user\""
      open_issue_label_list: "~\"reports::vsd\""
```

### Configure the access token

To securely store the access token for the API calls, you must configure it as a [CI/CD variable](https://docs.gitlab.com/ee/ci/variables/#for-a-project).

1. Open the GitLab project containing the `.gitlab-ci.yml` file.
1. Go to `Settings > CI/CD`.
1. Open the `Variables` section.
1. Add a new variable (make sure masking is enabled) called `API_KEY`.
1. Add the access token value, then select `Add variable`.

### Test the pipeline

1. Open the GitLab project containing the `.gitlab-ci.yml` file.
1. Go to `Build > Pipelines`.
1. Select `Run pipeline`.
1. Add a variable: `RUN_VSD_REPORT`, value: `true`
1. Select `Run pipeline`.
1. Inspect the pipeline output and verify that the issue with the metrics is opened correctly.

### Set up a pipeline schedulSet up a pipeline schedule

1. Open the GitLab project containing the `.gitlab-ci.yml` file.
1. Go to `Build > Pipeline schedules`.
1. Select `Create a new pipeline schedule`.
1. Configure the interval pattern and add a variable `RUN_VSD_REPORT` with the value: `true`.
1. Select `Create pipeline schedule`.

### Configuration options

The following YAML configuration options are available:

|Variable name|Description|Required|
|-|-|-|
|`vsd_config_project_url`|The full URL to the GitLab project where the VSD YAML file is located|Yes|
|`access_token`|API access token that covers all project or group paths defined in the VSD YAML file|Yes|
|`dashboard_yaml_path`|Use a custom path for the VSD YAML file within the project repo. Defaults to: `.gitlab/analytics/dashboards/value_streams/value_streams.yaml`|No|
|`open_issue_in_project_path`|Project path where a new confidential issue is opened after the report generation|No|
|`open_issue_assignee_list`|List of usernames the opened issue is assigned to. Example: `@username1 @username2`|No|
|`open_issue_label_list`|List of labels for the opened issue. Example: `~"backend" ~"workflow::review"`|No|
|`open_issue_title_prefix`|Custom text for the issue title. Defaults to the configured dashboard title.|No|

## Usage (invoking the tool directly, only for development)

The tool can be configured using the following environment variables:

|Variable name|Description|Required|
|-|-|-|
|`VSD_CONFIG_PROJECT_URL`|The full URL to the GitLab project where the Value Streams Dashboard YAML file is located|Yes|
|`ACCESS_TOKEN`|API access token that covers all project or group paths defined in the VSD YAML file|Yes|
|`DASHBOARD_YAML_PATH`|Use a custom path for the VSD YAML file within the project repo. Defaults to: `.gitlab/analytics/dashboards/value_streams/value_streams.yaml`|No|
|`OPEN_ISSUE_IN_PROJECT_PATH`|Project path where a new confidential issue is opened after the report generation|No|
|`OPEN_ISSUE_ASSIGNEE_LIST`|List of usernames where the opened issue is assigned to. Example: `@username1 @username2`|No|
|`OPEN_ISSUE_LABEL_LIST`|List of labels for the opened issue. Example: `~"backend" ~"workflow::review"`|No|
|`OPEN_ISSUE_TITLE_PREFIX`|Custom text for the issue title. Defaults to the configured dashboard title|No|

To invoke the tool, execute the following command:

```bash
VSD_CONFIG_PROJECT_URL=https://gitlab.com/group1/project1 ACCESS_TOKEN=YOUR_TOKEN bin/report

cat report.md
```

Configure the tool to open an issue:

```bash
OPEN_ISSUE_ASSIGNEE_LIST='@user1 @root'
OPEN_ISSUE_LABEL_LIST='~"backend" ~"my::label"'
OPEN_ISSUE_IN_PROJECT_PATH=group/project
VSD_CONFIG_PROJECT_URL=http://localhost:3000/group/project
ACCESS_TOKEN=YOUR_TOKEN
bin/report
```
