# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ValueStreamDashboard::Formatters::GitLabFlavoredMarkdown do
  subject(:output) { described_class.new(metrics:).format }

  let(:nil_days) { ValueStreamDashboard::Column.new(raw_value: nil, config: { format: 'days' }) }
  let(:zero_days) { ValueStreamDashboard::Column.new(raw_value: 0, config: { format: 'days' }) }
  let(:ten_days) { ValueStreamDashboard::Column.new(raw_value: 10, config: { format: 'days' }) }
  let(:fifteen_days) { ValueStreamDashboard::Column.new(raw_value: 15, config: { format: 'days' }) }

  let(:headers) do
    [
      'Group Name',
      [DateTime.new(2023, 7, 1).utc.beginning_of_day, DateTime.new(2023, 7, 31).utc.end_of_day],
      [DateTime.new(2023, 6, 1).utc.beginning_of_day, DateTime.new(2023, 6, 30).utc.end_of_day],
      [DateTime.new(2023, 5, 1).utc.beginning_of_day, DateTime.new(2023, 5, 31).utc.end_of_day]
    ]
  end

  let(:data) do
    [
      ['Metric Name', ten_days, fifteen_days, ten_days]
    ]
  end

  let(:metrics) do
    [
      instance_double(ValueStreamDashboard::MetricsCollector, headers:, data:)
    ]
  end

  it 'formats the markdown table' do
    expected_output = <<~MARKDOWN
      |Group Name|2023-07-01 - 2023-07-31|2023-06-01 - 2023-06-30|
      |-|-|-|
      |Metric Name|10 d {- -33.3% -}|15 d {+ +50.0% +}|
    MARKDOWN

    expect(output).to eq(expected_output)
  end

  context 'when two metrics are given' do
    let(:metrics) do
      [
        instance_double(ValueStreamDashboard::MetricsCollector, headers:, data:),
        instance_double(ValueStreamDashboard::MetricsCollector, headers:, data:)
      ]
    end

    it 'leaves an empty line between the tables' do
      expected_output = <<~MARKDOWN
        |Group Name|2023-07-01 - 2023-07-31|2023-06-01 - 2023-06-30|
        |-|-|-|
        |Metric Name|10 d {- -33.3% -}|15 d {+ +50.0% +}|

        |Group Name|2023-07-01 - 2023-07-31|2023-06-01 - 2023-06-30|
        |-|-|-|
        |Metric Name|10 d {- -33.3% -}|15 d {+ +50.0% +}|
      MARKDOWN

      expect(output).to eq(expected_output)
    end
  end

  context 'when percentage calculation was not possible due to 0 or nil values' do
    let(:data) do
      [
        ['Metric Name', zero_days, fifteen_days, ten_days]
      ]
    end

    it 'skips the percentage' do
      expect(output).to include('|Metric Name|0 d|15 d {+ +50.0% +}|')
    end

    context 'when the previous period was 0' do
      let(:data) do
        [
          ['Metric Name', fifteen_days, zero_days, ten_days]
        ]
      end

      it 'skips the percentage' do
        expect(output).to include('|Metric Name|15 d|0 d|')
      end
    end

    context 'when nil values are present returned' do
      let(:data) do
        [
          ['Metric Name', nil_days, zero_days, ten_days]
        ]
      end

      it 'skips rendering the value' do
        expect(output).to include('|Metric Name||0 d|')
      end
    end

    context 'when the previous period was nil' do
      let(:data) do
        [
          ['Metric Name', ten_days, nil_days, ten_days]
        ]
      end

      it 'skips rendering the value' do
        expect(output).to include('|Metric Name|10 d||')
      end
    end
  end
end
