# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ValueStreamDashboard::Config, :vcr do
  subject(:config) { described_class.build }

  it 'parses and sets the options correctly' do
    expect(config[:base_url]).to eq('https://gdk.localhost:3443')
    expect(config[:project_path]).to eq('gitlab-org/gitlab-shell')
  end

  describe '#dashboard_config' do
    it 'retrieves the dashboard config for the configured project' do
      expect(config.dashboard_config).to match(a_hash_including({
                                                                  'title' => 'Custom Dashboard title',
                                                                  'description' => 'Custom description'
                                                                }))
    end
  end
end
